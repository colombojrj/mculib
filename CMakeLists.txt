set(CMAKE_SYSTEM_NAME Generic)
set(CMAKE_SYSTEM_VERSION 1)
cmake_minimum_required(VERSION 3.25)

########################################
# Set here the cross-compilers and tools
set(CMAKE_C_COMPILER   arm-none-eabi-gcc)
set(CMAKE_CXX_COMPILER arm-none-eabi-g++)
set(CMAKE_ASM_COMPILER arm-none-eabi-gcc)
set(CMAKE_AR           arm-none-eabi-ar)
set(CMAKE_OBJCOPY      arm-none-eabi-objcopy)
set(CMAKE_OBJDUMP      arm-none-eabi-objdump)
set(SIZE               arm-none-eabi-size)
set(CMAKE_TRY_COMPILE_TARGET_TYPE STATIC_LIBRARY)
########################################

########################################
# Set here the used microcontroller
set(MCU STM32F103C8)
########################################

########################################
# Set the application source files here
set(SOURCES
	src/main.cpp
	src/sysmem.c
#	lib/hal/stm32/clock.cpp
#	lib/hal/stm32/interrupts.c
#	lib/utils/bit.c
#	lib/utils/fifo.c
#	lib/utils/list.c
#	lib/utils/math.c
#	lib/utils/time.c
)
########################################

########################################
# Set application include folders
include_directories("./src")
########################################

# Project settings
project(mculib C CXX ASM)
set(CMAKE_CXX_STANDARD 11)
set(CMAKE_C_STANDARD 11)

# Apply MCU settings
if (MCU MATCHES STM32F103C8)
	add_compile_options(-mfloat-abi=soft)
	add_compile_options(-mcpu=cortex-m3 -mthumb -mthumb-interwork)
	add_definitions(-DSTM32F1 -DSTM32F103C8)
endif()

# Apply general settings
add_compile_options(-Wall -Werror)
if("${CMAKE_BUILD_TYPE}" STREQUAL "Release")
    message(STATUS "Maximum optimization for speed")
    add_compile_options(-O3)
elseif("${CMAKE_BUILD_TYPE}" STREQUAL "RelWithDebInfo")
    message(STATUS "Maximum optimization for speed, debug info included")
    add_compile_options(-O3 -g)
elseif("${CMAKE_BUILD_TYPE}" STREQUAL "MinSizeRel")
    message(STATUS "Maximum optimization for size")
    add_compile_options(-Os)
else()
    message(STATUS "Minimal optimization, debug info included")
    add_compile_options(-Og -g3)
endif()

# Apply compiler options
add_compile_options($<$<COMPILE_LANGUAGE:C>:-ffunction-sections>)       # Place functions in their own sections
add_compile_options($<$<COMPILE_LANGUAGE:C>:-fdata-sections>)           # Place data in their own sections
add_compile_options($<$<COMPILE_LANGUAGE:CXX>:-ffunction-sections>)     # Place functions in their own sections
add_compile_options($<$<COMPILE_LANGUAGE:CXX>:-fdata-sections>)         # Place data in their own sections
add_compile_options($<$<COMPILE_LANGUAGE:CXX>:-fno-exceptions>)         # Disable handling exceptions
add_compile_options($<$<COMPILE_LANGUAGE:CXX>:-fno-rtti>)               # Disable generation of information about every class with virtual functions
add_compile_options($<$<COMPILE_LANGUAGE:CXX>:-fno-use-cxa-atexit>)     # Do not use __cxa_atexit for registering static destructors
add_compile_options($<$<COMPILE_LANGUAGE:CXX>:-fstack-usage>)           # Enable stack usage analysis

# Linker options
add_link_options(-Wl,-gc-sections,--print-memory-usage,-Map=${PROJECT_BINARY_DIR}/${PROJECT_NAME}.map)
add_link_options(--specs=nosys.specs --specs=nano.specs) # -nostdlib)
if(MCU MATCHES STM32F103C8)
	set(LINKER_SCRIPT ${CMAKE_SOURCE_DIR}/src/hal/stm32/vendor/STM32F103C8TX_FLASH.ld)
	add_link_options(-T ${LINKER_SCRIPT})
	add_link_options(-mcpu=cortex-m3 -mthumb -mthumb-interwork)
endif()

# Startup file
if (MCU MATCHES STM32F103C8)
	set(STARTUP_FILE ${CMAKE_SOURCE_DIR}/src/hal/stm32/vendor/startup_stm32f103c8tx.s)
endif()

# Add the application
add_executable(${PROJECT_NAME}.elf ${SOURCES} ${STARTUP_FILE} ${LINKER_SCRIPT})
set(HEX_FILE ${PROJECT_BINARY_DIR}/${PROJECT_NAME}.hex)
set(BIN_FILE ${PROJECT_BINARY_DIR}/${PROJECT_NAME}.bin)
add_custom_command(TARGET ${PROJECT_NAME}.elf POST_BUILD
        COMMAND ${CMAKE_OBJCOPY} -Oihex $<TARGET_FILE:${PROJECT_NAME}.elf> ${HEX_FILE}
        COMMAND ${CMAKE_OBJCOPY} -Obinary $<TARGET_FILE:${PROJECT_NAME}.elf> ${BIN_FILE}
        COMMENT "Building ${HEX_FILE}
Building ${BIN_FILE}")


