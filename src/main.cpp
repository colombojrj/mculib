#include <hal/gpio.hpp>

#ifdef __cplusplus
extern "C" {
#endif
    void _read() {}

    void _write() {}

    void _close() {}

    void _lseek() {}
#ifdef __cplusplus
}
#endif


int main()
{
    PC13::init(GpioMode::OutputPushPull);

	while (true)
	{
        PC13::write(0);
        PC13::write(1);
    }
}