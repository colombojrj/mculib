#ifndef MCULIB_ARCHITECTURE_GPIO_H_
#define MCULIB_ARCHITECTURE_GPIO_H_

/**
 ***********************************************************************************************************************
 * @file    stm32f103c8.hpp
 * @author  José Roberto Colombo Junior
 * @brief   Gpio API definitions.
 ***********************************************************************************************************************
 *
 * This library adds support to the microcontroller basic digital gpio. It tries to be the fastest as possible, avoiding
 * all if else statements possible.
 *
 * @attention
 *
 * This is free software licensed under GPL.
 *
 ***********************************************************************************************************************
 */

#include "detect.h"

#if defined(STM32F103C8)
#include "stm32/gpio_impl.hpp"
#endif

/**
 * @brief This function is automatically invoked when a interrupt is triggered by some pin. It is supposed that the user
 *        will implement this function.
 */
__weak void gpioPinCallback(uint16_t pin)
{
    UNUSED(pin);
}

#endif /* MCULIB_ARCHITECTURE_GPIO_H_ */
