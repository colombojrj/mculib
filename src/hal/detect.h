#ifndef ARCHITECTURE_DETECT_H
#define ARCHITECTURE_DETECT_H

#if defined ( __GNUC__ )
#include <bits/c++config.h>
#endif

#ifndef UNUSED
#define UNUSED(x) ((void)x)
#endif

#if defined ( __GNUC__ )
#ifndef __weak
#define __weak   __attribute__((weak))
#endif
#endif

#if defined(STM32F103C8Tx) || defined(STM32F103C8)
#ifndef STM32F103C8
#define STM32F103C8  ///< If using code generation engine such as CubeIDE/CubeMX, the F103C8 will be defined as STM32F103C8Tx.
#endif

/**
  \brief   Count leading zeros
  \details Counts the number of leading zeros of a data value.
  \param [in]  value  Value to count the leading zeros
  \return             number of leading zeros in value
 */
#define __CLZ             (uint8_t)__builtin_clz

#endif

#endif //ARCHITECTURE_DETECT_H
