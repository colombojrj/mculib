#ifndef ARCHITECTURE_STM32_SPI_H
#define ARCHITECTURE_STM32_SPI_H

#ifdef STM32F1
#include "stm32f1xx_hal.h"
#include "stm32f1xx_hal_spi.h"
#include "stm32f1xx_hal_def.h"
#endif

#endif //ARCHITECTURE_STM32_SPI_H
