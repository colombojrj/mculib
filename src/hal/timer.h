#ifndef ARCHITECTURE_TIMER_H_
#define ARCHITECTURE_TIMER_H_

#if defined(STM32F103xB)
#include "stm32f1xx_hal.h"
#include "stm32f1xx_hal_tim.h"
#include "stm32f1xx_hal_def.h"
#elif defined(STM32F303x8)
#elif defined(STM32F446xx)
#include "stm32f4xx_hal.h"
#include "stm32f4xx_hal_tim.h"
#include "stm32f4xx_hal_def.h"
#endif

#endif //ARCHITECTURE_TIMER_H_
