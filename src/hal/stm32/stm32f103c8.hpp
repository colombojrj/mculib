#ifndef MCULIB_ARCHITECTURE_STM32_MCU_STM32F103C8_H_
#define MCULIB_ARCHITECTURE_STM32_MCU_STM32F103C8_H_

/**
 ***********************************************************************************************************************
 * @file    stm32f103c8.hpp
 * @author  José Roberto Colombo Junior
 * @brief   STM32F103C8 definitions.
 ***********************************************************************************************************************
 *
 * @attention
 *
 * This is free software licensed under GPL.
 *
 ***********************************************************************************************************************
 */

#include "hal/detect.h"
#include "utils/types.h"
#include "utils/bit.h"

/**
 * @brief Defines all supported trigger to pin interrupts.
 * @warning This is hal dependent.
 */
typedef enum GpioTrigger {
    gpioTriggerNone = 0,  ///< Disabled.
    gpioTriggerFalling,   ///< Trigger only on falling edges.
    gpioTriggerRising,    ///< Trigger only on rising edges.
    gpioTriggerBoth       ///< Trigger on rising and falling edges.
} GpioTrigger_t;

/**
 * @brief Defines all supported pull resistors.
 * @warning This is hal dependent.
 */
enum class GpioPull {
    None = 0, ///< Pull resistor disabled.
    Down = 0, ///< Pull-down resistor.
    Up   = 1, ///< Pull-up resistor.
};

/**
 * This enum maps the available gpio pin mode. In this mcu it corresponds to the CNF1, CNF0, MODE1 and MODE0
 * bits in the port configuration registers (CRH and CRL). This information is available at tables 20 and 21
 * at page 160 of the datasheet.
 */
enum class GpioMode {
    InputAnalog            = 0b0000,  ///< The pin operates in input analog mode. This will disable most of the pin circuitry, what may be useful for power saving.
    OutputPushPull         = 0b0011,  ///< The pin operates in output mode, with a push pull driver.
    InputFloating          = 0b0100,  ///< The pin operates in input mode floating (without pull resistor).
    Input                  = 0b1000,  ///< The pin operates in input mode with pull resistor.
    OutputOpenDrain        = 0b0111,  ///< The pin operates in output mode, with an open drain driver.
    AlternatePushPull      = 0b1011,  ///< The pin is driven by another peripheral, with a push pull driver.
    AlternateOpenDrain     = 0b1111,  ///< The pin is driven by another peripheral, with an open drain driver.
    Mask                   = 0b1111,  ///< The mask bits to the configuration.
};

enum class GpioLock {
    Lck0,  ///< This bit is R/W, but the write operation only has some effect when LCKK is zero.
    Lck1,  ///< This bit is R/W, but the write operation only has some effect when LCKK is zero.
    Lck2,  ///< This bit is R/W, but the write operation only has some effect when LCKK is zero.
    Lck3,  ///< This bit is R/W, but the write operation only has some effect when LCKK is zero.
    Lck4,  ///< This bit is R/W, but the write operation only has some effect when LCKK is zero.
    Lck5,  ///< This bit is R/W, but the write operation only has some effect when LCKK is zero.
    Lck6,  ///< This bit is R/W, but the write operation only has some effect when LCKK is zero.
    Lck7,  ///< This bit is R/W, but the write operation only has some effect when LCKK is zero.
    Lck8,  ///< This bit is R/W, but the write operation only has some effect when LCKK is zero.
    Lck9,  ///< This bit is R/W, but the write operation only has some effect when LCKK is zero.
    Lck10,  ///< This bit is R/W, but the write operation only has some effect when LCKK is zero.
    Lck11,  ///< This bit is R/W, but the write operation only has some effect when LCKK is zero.
    Lck12,  ///< This bit is R/W, but the write operation only has some effect when LCKK is zero.
    Lck13,  ///< This bit is R/W, but the write operation only has some effect when LCKK is zero.
    Lck14,  ///< This bit is R/W, but the write operation only has some effect when LCKK is zero.
    Lck15,  ///< This bit is R/W, but the write operation only has some effect when LCKK is zero.
    /**
     * Lock key
     * This bit can be read anytime. It can only be modified using the Lock Key Writing Sequence.
     * 0: Port configuration lock key not active
     * 1: Port configuration lock key active. GPIOx_LCKR register is locked until an MCU reset occurs.
     *
     * LOCK key writing sequence:
     * Write 1
     * Write 0
     * Write 1
     * Read 0
     * Read 1 (this read is optional but confirms that the lock is active)
     *
     * @note During the LOCK Key Writing sequence, the value of LCK[15:0] must not change. Any error in the lock
     *       sequence will abort the lock.
     */
    Lckk = 16,
};

/**
 * Define the GpioRegs_t to this microcontroller.
 */
typedef struct GpioRegs {
	volatile uint32_t CRL;   ///< Port configuration register low.
	volatile uint32_t CRH;   ///< Port configuration register high.
	volatile uint32_t IDR;   ///< Port input data register.
	volatile uint32_t ODR;   ///< Port output data register.
	volatile uint32_t BSRR;  ///< Port bit set/reset register.
	volatile uint32_t BRR;   ///< Port bit reset register.
	volatile uint32_t LCKR;  ///< Port configuration lock register.
} GpioRegs_t;

typedef volatile GpioRegs_t* Gpio_t;

/**
  * @brief Alternate Function I/O register.
  */
typedef struct AfioRegs
{
    volatile uint32_t EVCR;       ///< Event control register.
    volatile uint32_t MAPR;       ///< AF remap and debug I/O configuration register.
    volatile uint32_t EXTICR[4];  ///< External interrupt configuration register.
    uint32_t RESERVED0;
    volatile uint32_t MAPR2;
} AfioRegs_t;

typedef AfioRegs_t* Afio_t;

typedef struct ExtiRegs
{
    volatile uint32_t IMR;
    volatile uint32_t EMR;
    volatile uint32_t RTSR;
    volatile uint32_t FTSR;
    volatile uint32_t SWIER;
    volatile uint32_t PR;
} ExtiRegs_t;

typedef volatile ExtiRegs_t* Exti_t;

enum class RccApb2ClockEnable {
    Afio = 0,
    Reserved1,
    PortA,
    PortB,
    PortC,
    PortD,
    PortE,
    Reserved7,
    Reserved8,
    Adc1,
    Adc2,
    Tim1,
    Spi1,
    Usart1,
    Reserved15,
};

/**
  * @brief Reset and Clock Control
  */
typedef struct RccRegs
{
    volatile uint32_t CR;
    volatile uint32_t CFGR;
    volatile uint32_t CIR;
    volatile uint32_t APB2RSTR;
    volatile uint32_t APB1RSTR;
    volatile uint32_t AHBENR;
    volatile uint32_t APB2ENR;
    volatile uint32_t APB1ENR;
    volatile uint32_t BDCR;
    volatile uint32_t CSR;
} RccRegs_t;

typedef RccRegs_t* Rcc_t;

/*!< Interrupt Number Definition */
typedef enum
{
/******  Cortex-M3 Processor Exceptions Numbers ***************************************************/
    NonMaskableInt_IRQn         = -14,    /*!< 2 Non Maskable Interrupt                             */
    HardFault_IRQn              = -13,    /*!< 3 Cortex-M3 Hard Fault Interrupt                     */
    MemoryManagement_IRQn       = -12,    /*!< 4 Cortex-M3 Memory Management Interrupt              */
    BusFault_IRQn               = -11,    /*!< 5 Cortex-M3 Bus Fault Interrupt                      */
    UsageFault_IRQn             = -10,    /*!< 6 Cortex-M3 Usage Fault Interrupt                    */
    SVCall_IRQn                 = -5,     /*!< 11 Cortex-M3 SV Call Interrupt                       */
    DebugMonitor_IRQn           = -4,     /*!< 12 Cortex-M3 Debug Monitor Interrupt                 */
    PendSV_IRQn                 = -2,     /*!< 14 Cortex-M3 Pend SV Interrupt                       */
    SysTick_IRQn                = -1,     /*!< 15 Cortex-M3 System Tick Interrupt                   */

/******  STM32 specific Interrupt Numbers *********************************************************/
    WWDG_IRQn                   = 0,      /*!< Window WatchDog Interrupt                            */
    PVD_IRQn                    = 1,      /*!< PVD through EXTI Line detection Interrupt            */
    TAMPER_IRQn                 = 2,      /*!< Tamper Interrupt                                     */
    RTC_IRQn                    = 3,      /*!< RTC global Interrupt                                 */
    FLASH_IRQn                  = 4,      /*!< FLASH global Interrupt                               */
    RCC_IRQn                    = 5,      /*!< RCC global Interrupt                                 */
    EXTI0_IRQn                  = 6,      /*!< EXTI Line0 Interrupt                                 */
    EXTI1_IRQn                  = 7,      /*!< EXTI Line1 Interrupt                                 */
    EXTI2_IRQn                  = 8,      /*!< EXTI Line2 Interrupt                                 */
    EXTI3_IRQn                  = 9,      /*!< EXTI Line3 Interrupt                                 */
    EXTI4_IRQn                  = 10,     /*!< EXTI Line4 Interrupt                                 */
    DMA1_Channel1_IRQn          = 11,     /*!< DMA1 Channel 1 global Interrupt                      */
    DMA1_Channel2_IRQn          = 12,     /*!< DMA1 Channel 2 global Interrupt                      */
    DMA1_Channel3_IRQn          = 13,     /*!< DMA1 Channel 3 global Interrupt                      */
    DMA1_Channel4_IRQn          = 14,     /*!< DMA1 Channel 4 global Interrupt                      */
    DMA1_Channel5_IRQn          = 15,     /*!< DMA1 Channel 5 global Interrupt                      */
    DMA1_Channel6_IRQn          = 16,     /*!< DMA1 Channel 6 global Interrupt                      */
    DMA1_Channel7_IRQn          = 17,     /*!< DMA1 Channel 7 global Interrupt                      */
    ADC1_2_IRQn                 = 18,     /*!< ADC1 and ADC2 global Interrupt                       */
    USB_HP_CAN1_TX_IRQn         = 19,     /*!< USB Device High Priority or CAN1 TX Interrupts       */
    USB_LP_CAN1_RX0_IRQn        = 20,     /*!< USB Device Low Priority or CAN1 RX0 Interrupts       */
    CAN1_RX1_IRQn               = 21,     /*!< CAN1 RX1 Interrupt                                   */
    CAN1_SCE_IRQn               = 22,     /*!< CAN1 SCE Interrupt                                   */
    EXTI9_5_IRQn                = 23,     /*!< External Line[9:5] Interrupts                        */
    TIM1_BRK_IRQn               = 24,     /*!< TIM1 Break Interrupt                                 */
    TIM1_UP_IRQn                = 25,     /*!< TIM1 Update Interrupt                                */
    TIM1_TRG_COM_IRQn           = 26,     /*!< TIM1 Trigger and Commutation Interrupt               */
    TIM1_CC_IRQn                = 27,     /*!< TIM1 Capture Compare Interrupt                       */
    TIM2_IRQn                   = 28,     /*!< TIM2 global Interrupt                                */
    TIM3_IRQn                   = 29,     /*!< TIM3 global Interrupt                                */
    TIM4_IRQn                   = 30,     /*!< TIM4 global Interrupt                                */
    I2C1_EV_IRQn                = 31,     /*!< I2C1 Event Interrupt                                 */
    I2C1_ER_IRQn                = 32,     /*!< I2C1 Error Interrupt                                 */
    I2C2_EV_IRQn                = 33,     /*!< I2C2 Event Interrupt                                 */
    I2C2_ER_IRQn                = 34,     /*!< I2C2 Error Interrupt                                 */
    SPI1_IRQn                   = 35,     /*!< SPI1 global Interrupt                                */
    SPI2_IRQn                   = 36,     /*!< SPI2 global Interrupt                                */
    USART1_IRQn                 = 37,     /*!< USART1 global Interrupt                              */
    USART2_IRQn                 = 38,     /*!< USART2 global Interrupt                              */
    USART3_IRQn                 = 39,     /*!< USART3 global Interrupt                              */
    EXTI15_10_IRQn              = 40,     /*!< External Line[15:10] Interrupts                      */
    RTC_Alarm_IRQn              = 41,     /*!< RTC Alarm through EXTI Line Interrupt                */
    USBWakeUp_IRQn              = 42,     /*!< USB Device WakeUp from suspend through EXTI Line Interrupt */
} IrqNumber;

constexpr uint32_t FLASH_ADDRESS      = 0x08000000UL;                    ///< FLASH base address in the alias region.
constexpr uint32_t FLASH_BANK1_END    = 0x0801FFFFUL;                    ///< FLASH END address of bank1.
constexpr uint32_t SRAM_ADDRESS       = 0x20000000UL;                    ///< SRAM base address in the alias region.
constexpr uint32_t PERIPH_ADDRESS     = 0x40000000UL;                    ///< Peripheral base address in the alias region.
constexpr uint32_t SRAM_BB_ADDRESS    = 0x22000000UL;                    ///< SRAM base address in the bit-band region.
constexpr uint32_t PERIPH_BB_ADDRESS  = 0x42000000UL;                    ///< Peripheral base address in the bit-band region.
constexpr uint32_t APB1PERIPH_ADDRESS = PERIPH_ADDRESS;
constexpr uint32_t APB2PERIPH_ADDRESS = PERIPH_ADDRESS + 0x00010000UL;
constexpr uint32_t AHBPERIPH_ADDRESS  = PERIPH_ADDRESS + 0x00020000UL;
constexpr uint32_t EXTI_ADDRESS       = APB2PERIPH_ADDRESS + 0x00000400UL;
constexpr uint32_t AFIO_ADDRESS       = APB2PERIPH_ADDRESS + 0x00000000UL;
constexpr uint32_t PORTA_ADDRESS      = APB2PERIPH_ADDRESS + 0x00000800UL;
constexpr uint32_t PORTB_ADDRESS      = APB2PERIPH_ADDRESS + 0x00000C00UL;
constexpr uint32_t PORTC_ADDRESS      = APB2PERIPH_ADDRESS + 0x00001000UL;
constexpr uint32_t PORTD_ADDRESS      = APB2PERIPH_ADDRESS + 0x00001400UL;

constexpr uint32_t DMA1_ADDRESS       = AHBPERIPH_ADDRESS + 0x00000000UL;
constexpr uint32_t DMA1_CH1_ADDRESS   = AHBPERIPH_ADDRESS + 0x00000008UL;
constexpr uint32_t DMA1_CH2_ADDRESS   = AHBPERIPH_ADDRESS + 0x0000001CUL;
constexpr uint32_t DMA1_CH3_ADDRESS   = AHBPERIPH_ADDRESS + 0x00000030UL;
constexpr uint32_t DMA1_CH4_ADDRESS   = AHBPERIPH_ADDRESS + 0x00000044UL;
constexpr uint32_t DMA1_CH5_ADDRESS   = AHBPERIPH_ADDRESS + 0x00000058UL;
constexpr uint32_t DMA1_CH6_ADDRESS   = AHBPERIPH_ADDRESS + 0x0000006CUL;
constexpr uint32_t DMA1_CH7_ADDRESS   = AHBPERIPH_ADDRESS + 0x00000080UL;
constexpr uint32_t RCC_ADDRESS        = AHBPERIPH_ADDRESS + 0x00001000UL;
constexpr uint32_t CRC_ADDRESS        = AHBPERIPH_ADDRESS + 0x00003000UL;

Exti_t EXTI = (const Exti_t) EXTI_ADDRESS;
Afio_t AFIO = (const Afio_t) AFIO_ADDRESS;
Gpio_t PORTA = (const Gpio_t) PORTA_ADDRESS;
Gpio_t PORTB = (const Gpio_t) PORTB_ADDRESS;
Gpio_t PORTC = (const Gpio_t) PORTC_ADDRESS;
Gpio_t PORTD = (const Gpio_t) PORTD_ADDRESS;
Rcc_t RCC = (const Rcc_t) RCC_ADDRESS;

#endif /* MCULIB_ARCHITECTURE_STM32_MCU_STM32F103C8_H_ */
