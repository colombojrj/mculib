static const IRQn_Type gpioIrqTable[] = {
        EXTI0_IRQn,
        EXTI1_IRQn,
        EXTI2_IRQn,
        EXTI3_IRQn,
        EXTI4_IRQn,
        EXTI9_5_IRQn,
        EXTI9_5_IRQn,
        EXTI9_5_IRQn,
        EXTI9_5_IRQn,
        EXTI9_5_IRQn,
        EXTI15_10_IRQn,
        EXTI15_10_IRQn,
        EXTI15_10_IRQn,
        EXTI15_10_IRQn,
        EXTI15_10_IRQn,
        EXTI15_10_IRQn,
};

IRQn_Type gpioGetIrq(uint8_t pinNumber)
{
    return gpioIrqTable[pinNumber];
}

// Accordingly to the datasheet, more specifically at pages 207/1137, 212/1137, it is required to clear the EXTI's line
// pending bits before invoking the callback.

void EXTI0_IRQHandler(void)
{
    // If this function (EXTI0_IRQHandler) is exclusive, i.e., if it was invoked, then an interrupt was triggered and
    // it is not required to check if an interrupt has occurred.
    EXTI->PR = Bit0;
    gpioPin0Callback();
}

void EXTI1_IRQHandler(void)
{
    // If this function (EXTI1_IRQHandler) is exclusive, i.e., if it was invoked, then an interrupt was triggered and
    // it is not required to check if an interrupt has occurred.
    EXTI->PR = Bit1;
    gpioPin1Callback();
}

void EXTI2_IRQHandler(void)
{
    // If this function (EXTI0_IRQHandler) is exclusive, i.e., if it was invoked, then an interrupt was triggered and
    // it is not required to check if an interrupt has occurred.
    EXTI->PR = Bit2;
    gpioPin2Callback();
}

void EXTI3_IRQHandler(void)
{
    // If this function (EXTI3_IRQHandler) is exclusive, i.e., if it was invoked, then an interrupt was triggered and
    // it is not required to check if an interrupt has occurred.
    EXTI->PR = Bit3;
    gpioPin3Callback();
}

void EXTI4_IRQHandler(void)
{
    // If this function (EXTI4_IRQHandler) is exclusive, i.e., if it was invoked, then an interrupt was triggered and
    // it is not required to check if an interrupt has occurred.
    EXTI->PR = Bit4;
    gpioPin4Callback();
}

void EXTI9_5_IRQHandler(void)
{
    if ((EXTI->PR & Bit5) > 0)
    {
        EXTI->PR = Bit5;
        gpioPin5Callback();
    }

    if ((EXTI->PR & Bit6) > 0)
    {
        EXTI->PR = Bit6;
        gpioPin6Callback();
    }

    if ((EXTI->PR & Bit7) > 0)
    {
        EXTI->PR = Bit7;
        gpioPin7Callback();
    }

    if ((EXTI->PR & Bit8) > 0)
    {
        EXTI->PR = Bit8;
        gpioPin8Callback();
    }

    if ((EXTI->PR & Bit9) > 0)
    {
        EXTI->PR = Bit9;
        gpioPin9Callback();
    }
}

void EXTI15_10_IRQHandler(void)
{
    if ((EXTI->PR & Bit10) > 0)
    {
        EXTI->PR = Bit10;
        gpioPin10Callback();
    }

    if ((EXTI->PR & Bit11) > 0)
    {
        EXTI->PR = Bit11;
        gpioPin11Callback();
    }

    if ((EXTI->PR & Bit12) > 0)
    {
        EXTI->PR = Bit12;
        gpioPin12Callback();
    }

    if ((EXTI->PR & Bit13) > 0)
    {
        EXTI->PR = Bit13;
        gpioPin13Callback();
    }

    if ((EXTI->PR & Bit14) > 0)
    {
        EXTI->PR = Bit14;
        gpioPin14Callback();

    }

    if ((EXTI->PR & Bit15) > 0)
    {
        EXTI->PR = Bit15;
        gpioPin15Callback();
    }
}
