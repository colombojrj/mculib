#ifndef MCULIB_ARCHITECTURE_STM32_GPIO_HPP_
#define MCULIB_ARCHITECTURE_STM32_GPIO_HPP_

#include "utils/bit.h"

#if defined(STM32F103C8)
#include "stm32f103c8.hpp"
#endif

static inline void prvClockEnablePeripheral(const uint32_t portAddress)
{
    switch (portAddress)
    {
        case PORTA_ADDRESS:
            RCC->APB2ENR |= numberToBit(AS_U8(RccApb2ClockEnable::PortA));
            break;

        case PORTB_ADDRESS:
            RCC->APB2ENR |= numberToBit(AS_U8(RccApb2ClockEnable::PortB));
            break;

        case PORTC_ADDRESS:
            RCC->APB2ENR |= numberToBit(AS_U8(RccApb2ClockEnable::PortC));
            break;

        case PORTD_ADDRESS:
            RCC->APB2ENR |= numberToBit(AS_U8(RccApb2ClockEnable::PortD));
            break;

        default:
            break;
    }
}

static inline void prvSetInterruptTrigger(uint8_t pinNumber, GpioTrigger edge)
{
    const uint32_t pinPosition = numberToBit(pinNumber);

    // Set the rising and falling edge triggers
    switch (edge) {
        case gpioTriggerNone:
            EXTI->RTSR &= ~pinPosition;
            EXTI->FTSR &= ~pinPosition;
            break;

        case gpioTriggerRising:
            EXTI->RTSR |= pinPosition;
            EXTI->FTSR &= ~pinPosition;
            break;

        case gpioTriggerFalling:
            EXTI->RTSR &= ~pinPosition;
            EXTI->FTSR |= pinPosition;
            break;

        case gpioTriggerBoth:
            EXTI->RTSR |= pinPosition;
            EXTI->FTSR |= pinPosition;
            break;

        default:
            break;
    }
}

template<uint32_t setPortAddress, uint8_t setPinNumber>
struct DigitalPin {
    static const uint8_t pinNumber = setPinNumber;                  ///< Expose the pin number.
    static const uint16_t pinPosition = numberToBit(setPinNumber);  ///< Expose the pin position.

    /**
     * @brief This function configures a pin.
     * @param newMode is the new operation mode of the pin.
     */
    static inline void init(const GpioMode mode, const GpioPull pull=GpioPull::None)
    {
        const auto port = reinterpret_cast<Gpio_t>(setPortAddress);

#ifdef STM32F1
        /*
         * Select the correct control register. In this MCU,
         * the pins 0, 1, ..., 7 are at CRL and
         * the pins 8, 9, ..., 15 are at CRH.
         *
         * Therefore, if pinNumber is greater or equal to 8, then CRH should be used in place of CRL.
         */
        volatile uint32_t* const reg = &port->CRL + (pinNumber / 8u);

        // Each pin occupies 4 bits in its control register. Therefore, the offset is calculated as follows.
        const uint32_t offset = 4 * (pinNumber % 8);

        // Make sure to enable clock at the pin.
        prvClockEnablePeripheral(setPortAddress);

        // Configure the gpio writing the appropriate value to control register
        bitWriteRw(reg, AS_U8(mode), offset, AS_U8(GpioMode::Mask));

        // If required enable the desired pull resistor
        if (mode == GpioMode::Input)
        {
            write(AS_U8(pull));
        }
#endif
    }

    static inline bool isLocked()
    {
#if defined(STM32F1)
        const auto port = reinterpret_cast<Gpio_t>(setPortAddress);
        return (port->LCKR & numberToBit(AS_U8(GpioLock::Lckk))) && (port->LCKR & pinPosition);
#endif
    }

    static inline bool lock()
    {
#if defined(STM32F1)
        const auto port = reinterpret_cast<Gpio_t>(setPortAddress);
        port->LCKR = pinPosition | numberToBit(AS_U8(GpioLock::Lckk));
        port->LCKR = pinPosition;
        port->LCKR = pinPosition | numberToBit(AS_U8(GpioLock::Lckk));
        volatile uint32_t read0 = port->LCKR; UNUSED(read0);
        return port->LCKR & pinPosition;
#endif
    }

    static inline void unlock()
    {
        const auto port = reinterpret_cast<Gpio_t>(setPortAddress);
        UNUSED(port);
    }

    static inline void enableInterrupt(const GpioTrigger trigger, const uint32_t priority)
    {
        const auto port = reinterpret_cast<Gpio_t>(setPortAddress);
        UNUSED(port);
    }

    static inline void disableInterrupt()
    {
        const auto port = reinterpret_cast<Gpio_t>(setPortAddress);
        UNUSED(port);
    }

    static inline void maskInterrupt()
    {
#if defined(STM32F1)
        bitWriteRw(&EXTI->IMR, 0, setPinNumber, 0b1);
        bitWriteRw(&EXTI->EMR, 0, setPinNumber, 0b1);
#endif
    }

    static inline void unmaskInterrupt()
    {
#if defined(STM32F1)
        bitWriteRw(&EXTI->IMR, 1, setPinNumber, 0b1);
        bitWriteRw(&EXTI->EMR, 0, setPinNumber, 0b1);
#endif
    }

    /**
     * Returns the port registers.
     * @return the port registers.
     */
    static inline Gpio_t regs()
    {
        return reinterpret_cast<const Gpio_t>(setPortAddress);
    }

    /**
     * @brief Read the pin logic state.
     * @return the logic state of the pin, 1 or 0.
     *
     * @example A simple example demonstrating how to use this function.
     *
     * @code
     * PB0::init(GpioMode::Input);
     * int state = PB0::read();
     * @endcode
     */
    static inline uint16_t read()
    {
        const auto port = reinterpret_cast<Gpio_t>(setPortAddress);
        return (port->IDR & pinPosition) > 0;
    }

    /**
     * @brief Updates the pin output logic state.
     * @param level the new logic state of the pin. It can be 1 or 0.
     *
     * @example A simple example demonstrating how to use this function.
     *
     * @code
     * PB0::init(GpioMode::OutputPushPull);
     * PB0::write(0);
     * @endcode
     */
    static inline void write(const uint16_t level)
    {
        const auto port = regs();

        /*
         * This MCU allows for writing 1 or 0 without the need of a if else statement: the BSRR register. Any write to
         * the first 16 bits (i.e., the LSB) will make the respective pin goes high. On the other hand, writing to the
         * MSB will make the pin goes low.
         */
        port->BSRR = (pinPosition << (16 * (level == 0)));
    }
};

#if defined(STM32F103C8)
typedef DigitalPin<PORTA_ADDRESS,  0> PA0;
typedef DigitalPin<PORTA_ADDRESS,  1> PA1;
typedef DigitalPin<PORTA_ADDRESS,  2> PA2;
typedef DigitalPin<PORTA_ADDRESS,  3> PA3;
typedef DigitalPin<PORTA_ADDRESS,  4> PA4;
typedef DigitalPin<PORTA_ADDRESS,  5> PA5;
typedef DigitalPin<PORTA_ADDRESS,  6> PA6;
typedef DigitalPin<PORTA_ADDRESS,  7> PA7;
typedef DigitalPin<PORTA_ADDRESS,  8> PA8;
typedef DigitalPin<PORTA_ADDRESS,  9> PA9;
typedef DigitalPin<PORTA_ADDRESS, 10> PA10;
typedef DigitalPin<PORTA_ADDRESS, 11> PA11;
typedef DigitalPin<PORTA_ADDRESS, 12> PA12;
typedef DigitalPin<PORTA_ADDRESS, 13> PA13;
typedef DigitalPin<PORTA_ADDRESS, 14> PA14;
typedef DigitalPin<PORTA_ADDRESS, 15> PA15;
typedef DigitalPin<PORTB_ADDRESS,  0> PB0;
typedef DigitalPin<PORTB_ADDRESS,  1> PB1;
typedef DigitalPin<PORTB_ADDRESS,  2> PB2;
typedef DigitalPin<PORTB_ADDRESS,  3> PB3;
typedef DigitalPin<PORTB_ADDRESS,  4> PB4;
typedef DigitalPin<PORTB_ADDRESS,  5> PB5;
typedef DigitalPin<PORTB_ADDRESS,  6> PB6;
typedef DigitalPin<PORTB_ADDRESS,  7> PB7;
typedef DigitalPin<PORTB_ADDRESS,  8> PB8;
typedef DigitalPin<PORTB_ADDRESS,  9> PB9;
typedef DigitalPin<PORTB_ADDRESS, 10> PB10;
typedef DigitalPin<PORTB_ADDRESS, 11> PB11;
typedef DigitalPin<PORTB_ADDRESS, 12> PB12;
typedef DigitalPin<PORTB_ADDRESS, 13> PB13;
typedef DigitalPin<PORTB_ADDRESS, 14> PB14;
typedef DigitalPin<PORTB_ADDRESS, 15> PB15;
typedef DigitalPin<PORTC_ADDRESS, 13> PC13;
typedef DigitalPin<PORTC_ADDRESS, 14> PC14;
typedef DigitalPin<PORTC_ADDRESS, 15> PC15;
typedef DigitalPin<PORTD_ADDRESS,  0> PD0;
typedef DigitalPin<PORTD_ADDRESS,  1> PD1;
#endif

#endif //MCULIB_ARCHITECTURE_STM32_GPIO_HPP_
