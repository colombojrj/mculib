#ifndef UTILS_BIT_H_
#define UTILS_BIT_H_

/**
 ******************************************************************************
 * @file    bit.h
 * @author  José Roberto Colombo Junior
 * @brief   Header file of the bit utils.
 ******************************************************************************
 *
 * This file adds support to basic bit manipulation API.
 *
 * @attention
 *
 * This is free software licensed under GPL.
 *
 ******************************************************************************
 */

#include "utils/types.h"

#ifdef __cplusplus
extern "C" {
#endif

#define Bit0   (1ul <<  0)  ///< Bit 0.
#define Bit1   (1ul <<  1)  ///< Bit 1.
#define Bit2   (1ul <<  2)  ///< Bit 2.
#define Bit3   (1ul <<  3)  ///< Bit 3.
#define Bit4   (1ul <<  4)  ///< Bit 4.
#define Bit5   (1ul <<  5)  ///< Bit 5.
#define Bit6   (1ul <<  6)  ///< Bit 6.
#define Bit7   (1ul <<  7)  ///< Bit 7.
#define Bit8   (1ul <<  8)  ///< Bit 8.
#define Bit9   (1ul <<  9)  ///< Bit 9.
#define Bit10  (1ul << 10)  ///< Bit 10.
#define Bit11  (1ul << 11)  ///< Bit 11.
#define Bit12  (1ul << 12)  ///< Bit 12.
#define Bit13  (1ul << 13)  ///< Bit 13.
#define Bit14  (1ul << 14)  ///< Bit 14.
#define Bit15  (1ul << 15)  ///< Bit 15.
#define Bit16  (1ul << 16)  ///< Bit 16.
#define Bit17  (1ul << 17)  ///< Bit 17.
#define Bit18  (1ul << 18)  ///< Bit 18.
#define Bit19  (1ul << 19)  ///< Bit 19.
#define Bit20  (1ul << 20)  ///< Bit 20.
#define Bit21  (1ul << 21)  ///< Bit 21.
#define Bit22  (1ul << 22)  ///< Bit 22.
#define Bit23  (1ul << 23)  ///< Bit 23.
#define Bit24  (1ul << 24)  ///< Bit 24.
#define Bit25  (1ul << 25)  ///< Bit 25.
#define Bit26  (1ul << 26)  ///< Bit 26.
#define Bit27  (1ul << 27)  ///< Bit 27.
#define Bit28  (1ul << 28)  ///< Bit 28.
#define Bit29  (1ul << 29)  ///< Bit 29.
#define Bit30  (1ul << 30)  ///< Bit 30.
#define Bit31  (1ul << 31)  ///< Bit 31.

#define isBit(bit) ((bit % 2) == 0)

/**
 * @def numberToBit(number)
 * Computes an unsigned integer with only one bit set, rotated to the left "number" times.
 *
 * @param number is the number of left rotations to be made.
 */
#define numberToBit(number) (1 << number)

/**
 * Converts a bit to a number. Example: if given 0b1000 (Bit 3), then it will return 3. It will return 32 if error.
 *
 * @param bit a byte with only one bit set.
 * @return the number of the bit position (beginning from zero).
 */
uint8_t bitToNumber(uint32_t bit);

/**
 * @def bitWriteRw(size, reg, value, offset, mask)
 * Write value to the R/W reg of size. The value to be written is located at offset rotations to the left with the mask.
 *
 * @param reg is the register address. It must match with size. For example: if writing to a 8 bit register, then size must be uint8_t.
 * @param value is the value to be written.
 * @param offset is how many rotations to the left the value is located.
 * @param mask is the bits should be changed.
 */
#define bitWriteRw(reg, value, offset, mask) *reg=((*reg)&~((mask)<<(offset)))|(((value)&(mask))<<(offset))

/**
 * @def bitWriteRo(reg, value, offset, mask)
 * Write value to the R/O register. The value to be written is located at offset rotations to the left with the mask.
 *
 * @param reg is the register address. It must match with size. For example: if writing to a 8 bit register, then size must be uint8_t.
 * @param value is the value to be written.
 * @param offset is how many rotations to the left the value is located.
 * @param mask is the bits should be changed.
 */
#define bitWriteRo(reg, value, offset, mask) *reg=((value)&(mask))<<(offset)

/**
 * @def bitRead(reg, offset, mask)
 * Read from a register.
 *
 * @param value is the value to be written.
 * @param offset is how many rotations to the left the value is located.
 * @param mask is the bits should be changed.
 */
#define bitRead(reg, offset, mask) (((*reg)&(mask<<offset))>>(offset))

#ifdef __cplusplus
}
#endif

#endif /* UTILS_BIT_H_ */
